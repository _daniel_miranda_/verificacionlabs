#Lo que hace el código es realizar una suma que el usuario ingrese como un string.
#Debilidades: Un input distinto a una suma, que no contenga "+"
#             Si es una suma de más de 2 números, solo va a sumar los primeros 2 
 
user_input = input("Give me a sum expression: \n")
user_input_list = user_input.split("+")

left_value = user_input_list[0]
right_value = user_input_list[1]

assert eval(user_input) == (int(left_value) + int(right_value))

#para romper la funcionalidad: ingresar una resta como por ejemplo "2-2" ya que no se verifica el caso de que no se ingrese una suma.