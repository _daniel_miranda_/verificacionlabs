import unittest
from ICountToN import CountToNInterface
from count_to_n_for import AppendFor
from count_to_n_while import AppendWhile

class TestEncuentraLetra(unittest.TestCase):
    
    def setUp(self):
        self.n = 5
        
    def test_print_for(self):
        self.ToTest = AppendFor()
 
        msj = self.ToTest.count_to_n(0)
        self.assertEqual("1", msj)        
        
        msj = self.ToTest.count_to_n(self.n)
        self.assertEqual("1 2 3 4 5", msj)
        
    def test_print_while(self):
        self.ToTest = AppendWhile()
 
        msj = self.ToTest.count_to_n(0)
        self.assertEqual("1", msj)        
        
        msj = self.ToTest.count_to_n(self.n)
        self.assertEqual("1 2 3 4 5", msj)