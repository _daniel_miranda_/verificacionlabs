from ICountToN import CountToNInterface

class AppendWhile(CountToNInterface):

    def count_to_n(self, n):
        str = "1"
        i = 1
        
        if n > 0:
            while (i <= n-1):
                str = str+f" {i+1}"
                i = i+1
        return str
