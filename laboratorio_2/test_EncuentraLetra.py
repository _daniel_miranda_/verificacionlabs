import unittest
from EncuentraLetra import Lab2

class TestEncuentraLetra(unittest.TestCase):
    
    def setUp(self):
        self.letra = "g"
        self.str = "Un gato"
        self.ToTest = Lab2()
        
    def test_correct_index(self):
        msj = self.ToTest.EncuentraLaLetra(self.letra, self.str)
        self.assertEqual("Letra esta en index 3", msj)
        
    def test_empty_string(self):
        self.str = ""
        msj = self.ToTest.EncuentraLaLetra(self.letra, self.str)
        self.assertEqual("no se introdujo una oracion", msj)
    
    def test_not_found(self):
        self.letra = "w"
        msj = self.ToTest.EncuentraLaLetra(self.letra, self.str)
        self.assertEqual("letra w no se encontro", msj)